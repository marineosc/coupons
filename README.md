# 多京淘，云开发的优惠券领取小程序

- 已经对接好拼多多的接口
- 以后可能对接京东联盟和淘宝联盟的，需要对接其他平台可以在评论中留言，有空我写一下（不过短期还需要找工作，没时间写）

## 插件地址
[https://ext.dcloud.net.cn/plugin?id=3699](https://ext.dcloud.net.cn/plugin?id=3699)

## 使用说明
1. 下载导入项目，然后修改 uniCloud->cloundfunctions->common->config-common->index.js 中的 pin_client_id 和 pin_client_secret，这两个参数是拼多多ID和密钥，需要去申请
2. 项目用到 [uni-id](https://ext.dcloud.net.cn/plugin?id=2116)^3.0版本, 需要修改 uniCloud->cloundfunctions->common->uni-config-center->uni-id->config.json 中的配置,如果是发布微信小程序，需要修改里面的 mp-weixin 下的参数
3. 关联云服务空间，上传所有云函数
4. 初始化数据库 uniCloud->databse->db_init.json
5. 修改 mixins/app.js pdd_pid(媒体位id) 为你自己的，可以在多多进宝后台申请这个媒体位
6. [拼多多申请说明](https://jinbao.pinduoduo.com/qa-system?questionId=231)

## 预览
![小程序](https://junyiseo.com/wp-content/uploads/2020/12/gh_1fe8a4e95848_258.jpg)

## 其他说明
- 前端模板使用了这个 [电商模板，纯前端模板，可直接使用](https://ext.dcloud.net.cn/plugin?id=2630), 感谢 Mr丶小斌


## 开发日志
#### 2.0.1
- 适配 HBuilder X 3.1.18

#### 2.0.0
- 升级 uni-id 为 3.0+
- 修复 若干bug

#### 1.0.1
- 修复拼多多小程序的bug

#### 1.0.0
- 对接拼多多，发布小程序



