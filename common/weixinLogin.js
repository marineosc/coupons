function getWeixinCode() {
	return new Promise((resolve, reject) => {
		uni.login({
			provider: 'weixin',
			success(res) {
				resolve(res.code)
			},
			fail(err) {
				reject(new Error('微信登录失败'))
			}
		})
	})
}

export function loginByWeixin() {
	let token = uni.getStorageSync('uni_id_token')
	if (token) {
		return
	}
	getWeixinCode().then((code) => {
		return uniCloud.callFunction({
			name: 'user-center',
			data: {
				action: 'loginByWeixin',
				params: {
					code,
				}
			}
		})
	}).then((res) => {
		// uni.showModal({
		// 	showCancel: false,
		// 	content: JSON.stringify(res.result)
		// })
		if (res.result.code === 0) {
			uni.setStorageSync('uni_id_token', res.result.token)
			uni.setStorageSync('uni_id_token_expired', res.result.tokenExpired)
		}
	}).catch((e) => {
		console.error(e)
		uni.showModal({
			showCancel: false,
			content: '微信登录失败，请稍后再试'
		})
	})
}
