export default function invokeCloud(name, data) {
	
	return new Promise((resolve, reject) => {
			uniCloud.callFunction({
				name,
				data
			}).then(res => {
				resolve(res.result.data)
			}).catch(e => {
				console.log('请求报错',e)
				reject(e)
			})
	})
	
}