'use strict';
const {
  commonQuery
} = require('config-common')
const uniID = require('uni-id')

const url = 'https://gw-api.pinduoduo.com/api/router'

exports.main = async (event, context) => {
	//event为客户端上传的参数
	console.log('event', event)
	
	const {uid} = await uniID.checkToken(event.uniIdToken)
	
	delete event.uniIdToken
	let query = event
	
	query.custom_parameters = JSON.stringify({uid:uid})
	console.log('query', query)

	query = commonQuery(query)
	
	console.log(query)
	
	
	const res = await uniCloud.httpclient.request(url, {
	    method: 'GET',
	    data: query,
	    dataType: 'json'
	  })
	  
	
	//返回数据给客户端
	return res
};
