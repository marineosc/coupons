export default {
	data() {
		return {
			// 拼多多推广位id
			pdd_pid:'13857450_182101967'
		}
	},
	methods:{
		// 查询是否备案
		checkAuthority(pid, callback) {	
			this.$cloud('pdd', {
				type: 'pdd.ddk.member.authority.query',
				pid: pid
			}).then(res => {
				if (res.authority_query_response && res.authority_query_response.bind === 0) {
					// 获取授权
					this.$cloud('pdd', {
						type: 'pdd.ddk.rp.prom.url.generate',
						p_id_list: JSON.stringify([pid]),
						channel_type: 10,
						generate_we_app: true
					}).then(res => {
						let data = res.rp_promotion_url_generate_response.url_list[0].we_app_info
						uni.navigateToMiniProgram({
							appId: data.app_id,
							path: data.page_path
						})
					})
				} else {
					callback
				}
			})
		}
	}
}